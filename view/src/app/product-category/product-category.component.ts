import { Component, OnInit } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { HomeComponent } from 'src/app/home/home.component';
import { RestService } from 'src/app/service/rest.service';
import { UserServiceService } from 'src/app/service/user.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-product-category',
  templateUrl: './product-category.component.html',
  styleUrls: ['./product-category.component.css']
})
export class ProductCategoryComponent implements OnInit {

  public productList: any;
  private sortedAsc: boolean = true;
  constructor(private rest:RestService,private service:UserServiceService,private auth:AuthService,private router:Router) { }
category_name=HomeComponent.category;
  ngOnInit(): void {
    this.getProductsByCategory(HomeComponent.category)
  }

  addtoCart(item: any){
    this.service.addtoCart(item);
  }

  getProductsByCategory(category: string){
    this.rest.getProductByCategory(HomeComponent.category)
    .subscribe(res=>{
      this.productList = res;
      this.productList.forEach((a:any) =>{
        Object.assign(a,{quantity:1,total:a.price});
      })
    })
  }

  sortProducts(){
    this.productList = this.sortedAsc ? this.productList.sort((a: any, b: any) => (a.price > b.price) ? -1 : 1) : this.productList.sort((a: any, b: any) => (a.price < b.price) ? -1 : 1)
    this.sortedAsc = !this.sortedAsc
  }

  addToCart(item: any){
    if(this.isLoggedIn()){
      
      
      this.service.addtoCart(item);
    }else{
      alert("You must be logged in to add items in your cart!!!")
      this.router.navigate(['/login']);
    }
  }
 

  addToWishlist(item: any){
    if(this.isLoggedIn()){
      this.service.addToWishlist(item);
    }else{
      alert("You must be logged in to add items in your cart!!!")
      this.router.navigate(['/login']);
    }
  }

  isLoggedIn(){
    return this.auth.isAuthorized;
  }

}
